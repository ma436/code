import numpy as np
class bayesian4():
    """
    This class implements a bayesian network with 4 variables X,Y1,Y2,Y3. X is parent node
    """
    def __init__(self,p_x, means,sigma,card_X,card_Y, samples):
        """
        Create a x,y0,y1,y2 dimensional bayesian network with x as parent node
        which implies p(x,y0,y1,y2) = p(y2|y1,y0,x)*p(y1|y0,x)*p(y0|x)*p(x)
                                    = p(y2|x)*p(y1|x)*p(y0|x)*p(x)
        :param means:  array of length 3 containing means for the joint distributions
        :param sigma:  array of length 3 containing standard deviations
        :param card_Y: cardinality of child nodes.
        :param card_X: cardinality of parent node
        :param samples: training samples for distribution
        """
        # Declare p(x)
        self.p_x = p_x
        self.means = means
        self.sigma = sigma
        self.card_X = card_X
        self.card_Y = card_Y
        self.samples = samples

        # Declare channel distribution for the bayesian network

        ## p(y0|x)
        self.p_y0_given_x = np.zeros((card_X, card_Y))
        p_y0_given_x0 = np.random.normal(means[0], sigma[0], samples)
        p_y0_given_x1 = np.random.normal(means[1], sigma[0], samples)
        p_y0_given_x0 = np.histogram(p_y0_given_x0, card_Y, range=(-4.5, 4.5))
        p_y0_given_x1 = np.histogram(p_y0_given_x1, card_Y, range=(-4.5, 4.5))
        self.p_y0_given_x[0, :] = p_y0_given_x0[0] / p_y0_given_x0[0].sum()
        self.p_y0_given_x[1, :] = p_y0_given_x1[0] / p_y0_given_x1[0].sum()

        ## Broad cast p(y0|x) to x,y0,y1,y2 dimensional matrix
        p_y0_givenx_mat = np.repeat(self.p_y0_given_x[:,:,np.newaxis],self.card_Y,axis=2) # x0,y0 to x0,y0,y1
        p_y0_givenx_mat = np.repeat(p_y0_givenx_mat[:,:,:,np.newaxis],self.card_Y,axis=3) # x0,y0,y1 to x0,y0,y1,y2


        ## p(y1|x)
        self.p_y1_given_x = np.zeros((card_X, card_Y))
        p_y1_given_x0 = np.random.normal(means[0], sigma[1], samples)
        p_y1_given_x1 = np.random.normal(means[1], sigma[1], samples)
        p_y1_given_x0 = np.histogram(p_y1_given_x0, card_Y, range=(-4.5, 4.5))
        p_y1_given_x1 = np.histogram(p_y1_given_x1, card_Y, range=(-4.5, 4.5))
        self.p_y1_given_x[0, :] = p_y1_given_x0[0] / p_y1_given_x0[0].sum()
        self.p_y1_given_x[1, :] = p_y1_given_x1[0] / p_y1_given_x1[0].sum()

        ## Broad cast p(y1|x) to x,y0,y1,y2 dimensional matrix
        p_y1_givenx_mat = (np.repeat(self.p_y1_given_x[:, :, np.newaxis], self.card_Y, axis=2)).transpose(0,2,1)  # x0,y1 to x0,y0,y1
        p_y1_givenx_mat = np.repeat(p_y1_givenx_mat[:, :, :, np.newaxis], self.card_Y, axis=3)  # x0,y0,y1 to x0,y0,y1,y2

        ## p(y2|x)
        self.p_y2_given_x = np.zeros((card_X, card_Y))
        p_y2_given_x0 = np.random.normal(means[0], sigma[2], samples)
        p_y2_given_x1 = np.random.normal(means[1], sigma[2], samples)
        p_y2_given_x0 = np.histogram(p_y2_given_x0, card_Y, range=(-4.5, 4.5))
        p_y2_given_x1 = np.histogram(p_y2_given_x1, card_Y, range=(-4.5, 4.5))
        self.p_y2_given_x[0, :] = p_y2_given_x0[0] / p_y2_given_x0[0].sum()
        self.p_y2_given_x[1, :] = p_y2_given_x1[0] / p_y2_given_x1[0].sum()

        ## Broad cast p(y2|x) to x,y0,y1,y2 dimensional matrix
        p_y2_givenx_mat = (np.repeat(self.p_y2_given_x[:, :, np.newaxis], self.card_Y, axis=2)).transpose(0, 2,1)  # x0,y2 to x0,y0,y2
        p_y2_givenx_mat = np.repeat(p_y2_givenx_mat[:, :, :, np.newaxis], self.card_Y,axis=3).transpose(0,1,3,2)  # x0,y0,y2 to x0,y0,y1,y2

        ##Broad cast p(x) tp x,y0,y1,y2
        p_x_mat = np.repeat(p_x[:,np.newaxis], self.card_Y, axis=1) # x0 to x0,y0
        p_x_mat = np.repeat(p_x_mat[:,:, np.newaxis], self.card_Y, axis=2) # x0,y0 to x0,y0,y1
        p_x_mat = np.repeat(p_x_mat[:,:,:, np.newaxis], self.card_Y, axis=3) # x0,y0,y1 to x0,y0,y1,y2

        ## p(x,y0,y1,y2)
        self.p_x_and_ybar = p_y2_givenx_mat * p_y1_givenx_mat * p_y0_givenx_mat * p_x_mat



class quantizer:
    def __init__(self, card_T,card_Y):
        N_ones = np.floor(card_Y / card_T)
        p_tmp = np.zeros([card_T, card_Y])
        for run in range(0, card_T):
            ptr = range(run * int(N_ones), min(((run + 1) * int(N_ones)), card_Y))
            p_tmp[run, ptr] = 1
        if ptr[-1] < card_Y:
            p_tmp[run, range(ptr.stop, np.size(p_tmp, 1))] = 1
        self.p_z_given_y = p_tmp.T