import numpy as np
# Procedure for debugging
# --Check dimensions allignment
# --Check sum() function. Sum should be performed for higher axis first

def MI(p_x_and_y):

    Nx,Ny = np.shape(p_x_and_y)
    p_x = np.repeat((p_x_and_y.sum(axis=1))[:,np.newaxis], Ny,axis = 1)
    p_y = (np.repeat((p_x_and_y.sum(axis=0))[:, np.newaxis], Nx, axis=1)).transpose(1,0)

    tm0 = np.nan_to_num(np.log2(p_x_and_y)) - np.nan_to_num(np.log2(p_y)) - np.nan_to_num(np.log2(p_x))
    I_XY = (p_x_and_y*tm0).sum()
    return I_XY

def DKL(P,Q):
    """
    :param P: Probability distribution along 0 axis
    :param Q: Probability Distribution along 0 axis
    :return: DKL vector of KL divergences
    """
    tmp = np.where(P != 0, P * np.log2(P/(Q+1e-31)), 0)
    DKL = np.sum(tmp,axis=0)
    DKL = np.sum(DKL)/np.size(DKL)
    return DKL

class aibnode():
    def __init__(self, m, p_x_and_ybar, beta, epsilon, p_zm_given_ym):

        self.p_z0_given_y0 = p_zm_given_ym[0,:]
        self.p_z1_given_y1 = p_zm_given_ym[1,:]
        self.p_z2_given_y2 = p_zm_given_ym[2,:]
        self.card_Y,self.card_T = p_zm_given_ym[0].shape
        self.card_X,card_y0,card_y1,card_y2 = p_x_and_ybar.shape

        #Convergence parameter for iterative algorithm
        epsiter = epsilon + 1 # Set epsiter higher than convergence threhold epsilon to make loop run atleast once
        while (epsiter > epsilon):
            #initialize p(t|y)
            p_z0_given_y0 = self.p_z0_given_y0
            p_z_given_y0mat3 = np.repeat(p_z0_given_y0[:,:,np.newaxis],self.card_Y,axis = 2).transpose(0,2,1)#y0,y1,z0
            p_z_given_y0mat4 = np.repeat(p_z_given_y0mat3[:,:,:,np.newaxis],self.card_T,axis = 3) #y0,y1,z0,z1

            p_z_given_y1mat3 = (np.repeat(self.p_z1_given_y1[:,:,np.newaxis],self.card_Y,axis = 2).transpose(0,2,1)).transpose(1,0,2)
            p_z_given_y1mat4 = (np.repeat(p_z_given_y1mat3[:,:,:, np.newaxis],self.card_T,axis = 3)).transpose(0,1,3,2)#y0,y1,z0,z1

            p_z_given_y2mat3 = (np.repeat(self.p_z2_given_y2[:, :, np.newaxis], self.card_Y, axis=2).transpose(0, 2, 1)).transpose(1, 0, 2)#y0,y2,z2
            p_z_given_y2mat4 = (np.repeat(p_z_given_y2mat3[:, :,:, np.newaxis], self.card_T, axis=3).transpose(0,1,3,2)) # y0,y2,z0,z2
            p_z_given_y2mat5 = ((np.repeat(p_z_given_y2mat4[:, :,:,:, np.newaxis], self.card_Y, axis=4).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)).transpose(0,2,1,3,4) # y0,y1,y2,z0,z2
            p_z_given_y2mat6 = (np.repeat(p_z_given_y2mat5[:, :, :, :,:, np.newaxis], self.card_T,axis=5)).transpose(0,1,2,3,5,4)  # y0,y1,y2,z0,z1,z2

            #Construct p(z|ybar) out of p(zm|ym)
            p_z0z1_given_y0y1 = p_z_given_y0mat4 * p_z_given_y1mat4 #y0,y1,z0,z1
            p_z0z1_given_y0y15 = (np.repeat(p_z0z1_given_y0y1[:, :, :,:, np.newaxis], self.card_Y, axis=4).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)#y0,y1,y2,z0,z1
            p_z0z1_given_y0y16 = np.repeat(p_z0z1_given_y0y15[:,:,:,:,:,np.newaxis],self.card_T,axis = 5)#y0,y1,y2,z0,z1,z2

            self.p_z_given_ybar = p_z0z1_given_y0y16 * p_z_given_y2mat6

            # Calculate p(x)
            self.p_x = ((p_x_and_ybar.sum(axis=3)).sum(axis = 2)).sum(axis = 1)
            p_xmat = np.repeat(np.reshape(self.p_x,(2,1)),self.card_Y,axis =1)
            p_xmat = np.repeat(p_xmat[:,:,np.newaxis],self.card_Y,axis =2)
            p_xmat = np.repeat(p_xmat[:, :,:, np.newaxis], self.card_Y, axis=3)
            p_xmat = np.repeat(p_xmat[:, :, :,:, np.newaxis], self.card_T, axis=4)
            p_xmat = np.repeat(p_xmat[:, :, :, :,:, np.newaxis], self.card_T, axis=5)
            p_xmat = np.repeat(p_xmat[:, :, :, :,:,:, np.newaxis], self.card_T, axis=6)

            # calculate p(ybar|x)
            self.p_ybar_given_x = p_x_and_ybar/(p_xmat[:,:,:,:,0,0,0])

            #calculate p(x,ynar,z)
            self.p_x_ybar_z = ((((((np.repeat(self.p_z_given_ybar[:, :, :, :,:,:,np.newaxis], self.card_X, axis=6)).transpose(0,1,2,3,4,6,5)).transpose(0,1,2,3,5,4,6)).transpose(0,1,2,4,3,5,6)).transpose(0,1,3,2,4,5,6)).transpose(0,2,1,3,4,5,6)).transpose(1,0,2,3,4,5,6)
            p_x_and_ybarmat = np.repeat(p_x_and_ybar[:, :, :, :, np.newaxis], self.card_T, axis=4)
            p_x_and_ybarmat = np.repeat(p_x_and_ybarmat[:, :, :, :, :, np.newaxis], self.card_T, axis=5)
            p_x_and_ybarmat = np.repeat(p_x_and_ybarmat[:, :, :, :, :, :, np.newaxis], self.card_T, axis=6)
            self.p_x_ybar_z  = self.p_x_ybar_z * p_x_and_ybarmat

            #calculate p(x|zm!=m,ym)
            p_zbarm_ym_x = ((self.p_x_ybar_z.sum(axis=4)).sum(axis=3)).sum(axis=2) #x,y0,,z1,z2
            p_zbarm_ym = p_zbarm_ym_x.sum(axis=0) #y0,z1,z2
            p_zbarm_ym = (((np.repeat(p_zbarm_ym[:,:,:,np.newaxis],self.card_X,axis =3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3) #x,y0,z1,z2
            p_x_given_zbarm_ym = np.nan_to_num((p_zbarm_ym_x/p_zbarm_ym),nan=0,posinf=0) #x,y0,z1,z2
            p_x_given_zbarm_ym = ((np.repeat(p_x_given_zbarm_ym[:,:,:,:,np.newaxis],self.card_T,axis=4)).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)# Repeat for z0 and allign as x,y0,z0,z1,z2

            #Calculate DKL
            p_x_zbar = ((self.p_x_ybar_z.sum(axis=3)).sum(axis=2)).sum(axis=1) #x,z0,,z1,z2
            p_zbar3d = p_x_zbar.sum(axis=0) #z0,z1,z2
            p_zbar = (((np.repeat(p_zbar3d[:,:,:,np.newaxis],self.card_X,axis = 3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3) #x,z0,z1,z2
            p_x_given_zbar = np.nan_to_num((p_x_zbar/p_zbar),nan=0,posinf=0)#x,z0,z1,z2

            p_x_given_zbar = (((np.repeat(p_x_given_zbar[:,:,:,:,np.newaxis],self.card_Y,axis = 4)).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)).transpose(0,2,1,3,4) #x,y0,z0,z1,z2

            dkl = np.nan_to_num((np.log2(np.nan_to_num((p_x_given_zbarm_ym/p_x_given_zbar),nan=0,posinf=0))),nan=0,posinf=0)
            dkl = (dkl*p_x_given_zbarm_ym).sum(axis=0) #y0,z0,z1,z2
            #Calculate p(zm!=m|zm,ym)
            p_zbar_ym = ((self.p_x_ybar_z.sum(axis=3)).sum(axis=2)).sum(axis=0) #y0,z0,z1,z2
            p_zm_ym = (p_zbar_ym.sum(axis=3)).sum(axis=2)
            p_zm_ym = np.repeat(p_zm_ym[:,:,np.newaxis],self.card_T,axis=2)
            p_zm_ym = np.repeat(p_zm_ym[:, :, :, np.newaxis], self.card_T, axis=3)#y0,z0,z1,z2
            p_zbarm_given_zmym = np.nan_to_num((p_zbar_ym/p_zm_ym),nan=0,posinf=0)

            #Calculate distance d(ym,zm)
            p_zbarm = p_zbar3d.sum(axis=0) #z1,z2
            p_zm = (p_zbar3d.sum(axis=2)).sum(axis=1)
            p_zm2d = (np.repeat(p_zm[:,np.newaxis],self.card_Y,axis=1)).transpose(1,0)#y0,z0
            p_zbarm = ((np.repeat(p_zbarm[:,:,np.newaxis],self.card_T,axis=2)).transpose(0,2,1)).transpose(1,0,2)#z0,z1,z2
            p_zm_given_zbarm = np.nan_to_num((p_zbar3d/p_zbarm),nan=0,posinf=0)
            d_ym_zm = np.nan_to_num(np.log2(p_zm_given_zbarm),nan=0,posinf=0)
            d_ym_zm = (((np.repeat(d_ym_zm[:,:,:,np.newaxis],self.card_Y, axis =3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3)#y0,z0,z1,z2
            d_ym_zm = beta*dkl-d_ym_zm
            d_ym_zm= ((p_zbarm_given_zmym * d_ym_zm).sum(axis=3)).sum(axis=2) #y0,z0
            d_ym_zm = np.nan_to_num((np.log2(p_zm2d)),nan=0,posinf=0) + d_ym_zm
            p_zm_ymnew = p_zm2d * np.exp(-d_ym_zm) #y0,z0
            normalization = p_zm_ymnew.sum(axis=1)
            normalization = np.repeat(normalization[:,np.newaxis],self.card_T,axis=1)
            p_zm_ymnew = p_zm_ymnew/normalization

            epsiter = ((np.square((p_zm_ymnew - p_z0_given_y0))).sum())/(self.card_T*self.card_Y)

            #Update quantizer
            self.p_z0_given_y0 = p_zm_ymnew

            # Calculate MI
            p_y0 = (((p_x_and_ybar).sum(axis=3)).sum(axis=2)).sum(axis=0)
            p_y0 = np.repeat(p_y0[:, np.newaxis], self.card_T, axis=1)
            self.rate = MI(self.p_z0_given_y0*p_y0)




class aibnode1():
    def __init__(self, m, p_x_and_ybar, beta, epsilon, p_zm_given_ym):
        self.p_z0_given_y0 = p_zm_given_ym[0,:]
        self.p_z1_given_y1 = p_zm_given_ym[1,:]
        self.p_z2_given_y2 = p_zm_given_ym[2,:]
        self.card_Y,self.card_T = p_zm_given_ym[0].shape
        self.card_X,card_y0,card_y1,card_y2 = p_x_and_ybar.shape

        # Convergence parameter for iterative algorithm
        epsiter = epsilon + 1  # Set epsiter higher than convergence threhold epsilon to make loop run atleast once
        while (epsiter > epsilon):
            #initialize p(t|y)
            p_z1_given_y1 = self.p_z1_given_y1
            p_z0_given_y0mat3 = np.repeat(self.p_z0_given_y0[:,:,np.newaxis],self.card_Y,axis = 2).transpose(0,2,1)#y0,y1,z0
            p_z0_given_y0mat4 = np.repeat(p_z0_given_y0mat3[:,:,:,np.newaxis],self.card_T,axis = 3) #y0,y1,z0,z1

            p_z1_given_y1mat3 = (np.repeat(p_z1_given_y1[:,:,np.newaxis],self.card_Y,axis = 2).transpose(0,2,1)).transpose(1,0,2)
            p_z1_given_y1mat4 = (np.repeat(p_z1_given_y1mat3[:,:,:, np.newaxis],self.card_T,axis = 3)).transpose(0,1,3,2)#y0,y1,z0,z1

            p_z_given_y2mat3 = (np.repeat(self.p_z2_given_y2[:, :, np.newaxis], self.card_Y, axis=2).transpose(0, 2, 1)).transpose(1, 0, 2)#y0,y2,z2
            p_z_given_y2mat4 = (np.repeat(p_z_given_y2mat3[:, :,:, np.newaxis], self.card_T, axis=3).transpose(0,1,3,2)) # y0,y2,z0,z2
            p_z_given_y2mat5 = ((np.repeat(p_z_given_y2mat4[:, :,:,:, np.newaxis], self.card_Y, axis=4).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)).transpose(0,2,1,3,4) # y0,y1,y2,z0,z2
            p_z_given_y2mat6 = (np.repeat(p_z_given_y2mat5[:, :, :, :,:, np.newaxis], self.card_T,axis=5)).transpose(0,1,2,3,5,4)  # y0,y1,y2,z0,z1,z2

            #Construct p(z|ybar) out of p(zm|ym)
            p_z0z1_given_y0y1 = p_z0_given_y0mat4 * p_z1_given_y1mat4 #y0,y1,z0,z1
            p_z0z1_given_y0y15 = (np.repeat(p_z0z1_given_y0y1[:, :, :,:, np.newaxis], self.card_Y, axis=4).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)#y0,y1,y2,z0,z1
            p_z0z1_given_y0y16 = np.repeat(p_z0z1_given_y0y15[:,:,:,:,:,np.newaxis],self.card_T,axis = 5)#y0,y1,y2,z0,z1,z2

            self.p_z_given_ybar = p_z0z1_given_y0y16 * p_z_given_y2mat6

            # Calculate p(x)
            self.p_x = ((p_x_and_ybar.sum(axis=3)).sum(axis = 2)).sum(axis = 1)
            p_xmat = np.repeat(np.reshape(self.p_x,(2,1)),self.card_Y,axis =1)
            p_xmat = np.repeat(p_xmat[:,:,np.newaxis],self.card_Y,axis =2)
            p_xmat = np.repeat(p_xmat[:, :,:, np.newaxis], self.card_Y, axis=3)
            p_xmat = np.repeat(p_xmat[:, :, :,:, np.newaxis], self.card_T, axis=4)
            p_xmat = np.repeat(p_xmat[:, :, :, :,:, np.newaxis], self.card_T, axis=5)
            p_xmat = np.repeat(p_xmat[:, :, :, :,:,:, np.newaxis], self.card_T, axis=6)

            # calculate p(ybar|x)
            self.p_ybar_given_x = p_x_and_ybar/(p_xmat[:,:,:,:,0,0,0])

            #calculate p(x,ynar,z)
            self.p_x_ybar_z = ((((((np.repeat(self.p_z_given_ybar[:, :, :, :,:,:,np.newaxis], self.card_X, axis=6)).transpose(0,1,2,3,4,6,5)).transpose(0,1,2,3,5,4,6)).transpose(0,1,2,4,3,5,6)).transpose(0,1,3,2,4,5,6)).transpose(0,2,1,3,4,5,6)).transpose(1,0,2,3,4,5,6)
            p_x_and_ybarmat = np.repeat(p_x_and_ybar[:, :, :, :, np.newaxis], self.card_T, axis=4)
            p_x_and_ybarmat = np.repeat(p_x_and_ybarmat[:, :, :, :, :, np.newaxis], self.card_T, axis=5)
            p_x_and_ybarmat = np.repeat(p_x_and_ybarmat[:, :, :, :, :, :, np.newaxis], self.card_T, axis=6)
            self.p_x_ybar_z  = self.p_x_ybar_z * p_x_and_ybarmat

            #calculate p(x|zm!=m,ym)
            p_zbar1_y1_x = ((self.p_x_ybar_z.sum(axis=5)).sum(axis=3)).sum(axis=1) #x,y1,,z0,z2
            p_zbar1_y1 = p_zbar1_y1_x.sum(axis=0) #y1,z0,z2
            p_zbar1_y1 = (((np.repeat(p_zbar1_y1[:,:,:,np.newaxis],self.card_X,axis =3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3) #x,y1,z0,z2
            p_x_given_zbar1_y1 = np.nan_to_num((p_zbar1_y1_x/p_zbar1_y1),nan=0,posinf=0) #x,y0,z1,z2
            p_x_given_zbar1_y1 = ((np.repeat(p_x_given_zbar1_y1[:,:,:,:,np.newaxis],self.card_T,axis=4)).transpose(0,1,2,4,3))# Repeat for z1 and allign to x,y0,z0,z1,z2

            #Calculate DKL
            p_x_zbar = ((self.p_x_ybar_z.sum(axis=3)).sum(axis=2)).sum(axis=1) #x,z0,,z1,z2
            p_zbar3d = p_x_zbar.sum(axis=0) #z0,z1,z2
            p_zbar = (((np.repeat(p_zbar3d[:,:,:,np.newaxis],self.card_X,axis = 3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3) #x,z0,z1,z2
            p_x_given_zbar = np.nan_to_num((p_x_zbar/p_zbar),nan=0,posinf=0)#x,z0,z1,z2

            p_x_given_zbar = (((np.repeat(p_x_given_zbar[:,:,:,:,np.newaxis],self.card_Y,axis = 4)).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)).transpose(0,2,1,3,4) #x,y1,z0,z1,z2

            dkl = np.nan_to_num((np.log2(np.nan_to_num((p_x_given_zbar1_y1/p_x_given_zbar),nan=0,posinf=0))),nan=0,posinf=0)
            dkl = (dkl*p_x_given_zbar1_y1).sum(axis=0) #y1,z0,z1,z2
            #Calculate p(zm!=m|zm,ym)
            p_zbar_ym1 = ((self.p_x_ybar_z.sum(axis=3)).sum(axis=1)).sum(axis=0) #y1,z0,z1,z2
            p_zm1_ym1 = (p_zbar_ym1.sum(axis=3)).sum(axis=1)
            p_zm1_ym1 = np.repeat(p_zm1_ym1[:,:,np.newaxis],self.card_T,axis=2).transpose(0,2,1) #Repeat for z0 and allign to y1,z0,z1
            p_zm1_ym1 = np.repeat(p_zm1_ym1[:, :, :, np.newaxis], self.card_T, axis=3)#Repeat for z2 and allign tp y1,z0,z1,z2
            p_zbarm_given_zmym = np.nan_to_num((p_zbar_ym1/p_zm1_ym1),nan=0,posinf=0)

            #Calculate distance d(ym,zm)
            p_zbarm = p_zbar3d.sum(axis=1) #z0,z2
            p_zm = (p_zbar3d.sum(axis=2)).sum(axis=0) #z1
            p_zm2d = (np.repeat(p_zm[:,np.newaxis],self.card_Y,axis=1)).transpose(1,0)#y1,z1
            p_zbarm = ((np.repeat(p_zbarm[:,:,np.newaxis],self.card_T,axis=2)).transpose(0,2,1))# Repeat for z1 and allign to z0,z1,z2
            p_zm_given_zbarm = np.nan_to_num((p_zbar3d/p_zbarm),nan=0,posinf=0) # z0 ,z1 z2
            d_ym_zm = np.nan_to_num(np.log2(p_zm_given_zbarm),nan=0,posinf=0) # z0,z1,z2
            d_ym_zm = (((np.repeat(d_ym_zm[:,:,:,np.newaxis],self.card_Y, axis =3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3)#y1,z0,z1,z2
            d_ym_zm = beta*dkl - d_ym_zm
            d_ym_zm= ((p_zbarm_given_zmym * d_ym_zm).sum(axis=3)).sum(axis=1) #y1,z1
            d_ym_zm = np.nan_to_num((np.log2(p_zm2d)),nan=0,posinf=0) + d_ym_zm
            p_zm_ymnew = p_zm2d * np.exp(-d_ym_zm) #y1,z1
            normalization = p_zm_ymnew.sum(axis=1)
            normalization = np.repeat(normalization[:,np.newaxis],self.card_T,axis=1)
            p_zm_ymnew = p_zm_ymnew/normalization

            epsiter = ((np.square((p_zm_ymnew - p_z1_given_y1))).sum())/(self.card_T*self.card_Y)

            #Update quantizer
            self.p_z1_given_y1 = p_zm_ymnew

            #Calculate MI
            p_y1 = (((p_x_and_ybar).sum(axis=3)).sum(axis=1)).sum(axis =0)
            p_y1 = np.repeat(p_y1[:,np.newaxis],self.card_T,axis=1)
            p_y1_z1 = self.p_z1_given_y1*p_y1
            self.rate = MI(p_y1_z1)

class aibnode2():
    def __init__(self, m, p_x_and_ybar, beta, epsilon, p_zm_given_ym):
        self.p_z0_given_y0 = p_zm_given_ym[0,:]
        self.p_z1_given_y1 = p_zm_given_ym[1,:]
        self.p_z2_given_y2 = p_zm_given_ym[2,:]
        self.card_Y,self.card_T = p_zm_given_ym[0].shape
        self.card_X,card_y0,card_y1,card_y2 = p_x_and_ybar.shape

        # Convergence parameter for iterative algorithm
        epsiter = epsilon + 1  # Set epsiter higher than convergence threhold epsilon to make loop run atleast once
        while (epsiter > epsilon):
            #initialize p(t|y)
            p_z2_given_y2 = self.p_z2_given_y2
            p_z0_given_y0mat3 = np.repeat(self.p_z0_given_y0[:,:,np.newaxis],self.card_Y,axis = 2).transpose(0,2,1)#y0,y1,z0
            p_z0_given_y0mat4 = np.repeat(p_z0_given_y0mat3[:,:,:,np.newaxis],self.card_T,axis = 3) #y0,y1,z0,z1

            p_z1_given_y1mat3 = (np.repeat(self.p_z1_given_y1[:,:,np.newaxis],self.card_Y,axis = 2).transpose(0,2,1)).transpose(1,0,2)
            p_z1_given_y1mat4 = (np.repeat(p_z1_given_y1mat3[:,:,:, np.newaxis],self.card_T,axis = 3)).transpose(0,1,3,2)#y0,y1,z0,z1

            p_z_given_y2mat3 = (np.repeat(p_z2_given_y2[:, :, np.newaxis], self.card_Y, axis=2).transpose(0, 2, 1)).transpose(1, 0, 2)#y0,y2,z2
            p_z_given_y2mat4 = (np.repeat(p_z_given_y2mat3[:, :,:, np.newaxis], self.card_T, axis=3).transpose(0,1,3,2)) # y0,y2,z0,z2
            p_z_given_y2mat5 = ((np.repeat(p_z_given_y2mat4[:, :,:,:, np.newaxis], self.card_Y, axis=4).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)).transpose(0,2,1,3,4) # y0,y1,y2,z0,z2
            p_z_given_y2mat6 = (np.repeat(p_z_given_y2mat5[:, :, :, :,:, np.newaxis], self.card_T,axis=5)).transpose(0,1,2,3,5,4)  # y0,y1,y2,z0,z1,z2

            #Construct p(z|ybar) out of p(zm|ym)
            p_z0z1_given_y0y1 = p_z0_given_y0mat4 * p_z1_given_y1mat4 #y0,y1,z0,z1
            p_z0z1_given_y0y15 = (np.repeat(p_z0z1_given_y0y1[:, :, :,:, np.newaxis], self.card_Y, axis=4).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)#y0,y1,y2,z0,z1
            p_z0z1_given_y0y16 = np.repeat(p_z0z1_given_y0y15[:,:,:,:,:,np.newaxis],self.card_T,axis = 5)#y0,y1,y2,z0,z1,z2

            self.p_z_given_ybar = p_z0z1_given_y0y16 * p_z_given_y2mat6

            # Calculate p(x)
            self.p_x = ((p_x_and_ybar.sum(axis=3)).sum(axis = 2)).sum(axis = 1)
            p_xmat = np.repeat(np.reshape(self.p_x,(2,1)),self.card_Y,axis =1)
            p_xmat = np.repeat(p_xmat[:,:,np.newaxis],self.card_Y,axis =2)
            p_xmat = np.repeat(p_xmat[:, :,:, np.newaxis], self.card_Y, axis=3)
            p_xmat = np.repeat(p_xmat[:, :, :,:, np.newaxis], self.card_T, axis=4)
            p_xmat = np.repeat(p_xmat[:, :, :, :,:, np.newaxis], self.card_T, axis=5)
            p_xmat = np.repeat(p_xmat[:, :, :, :,:,:, np.newaxis], self.card_T, axis=6)

            # calculate p(ybar|x)
            self.p_ybar_given_x = p_x_and_ybar/(p_xmat[:,:,:,:,0,0,0])

            #calculate p(x,ynar,z)
            self.p_x_ybar_z = ((((((np.repeat(self.p_z_given_ybar[:, :, :, :,:,:,np.newaxis], self.card_X, axis=6)).transpose(0,1,2,3,4,6,5)).transpose(0,1,2,3,5,4,6)).transpose(0,1,2,4,3,5,6)).transpose(0,1,3,2,4,5,6)).transpose(0,2,1,3,4,5,6)).transpose(1,0,2,3,4,5,6)
            p_x_and_ybarmat = np.repeat(p_x_and_ybar[:, :, :, :, np.newaxis], self.card_T, axis=4)
            p_x_and_ybarmat = np.repeat(p_x_and_ybarmat[:, :, :, :, :, np.newaxis], self.card_T, axis=5)
            p_x_and_ybarmat = np.repeat(p_x_and_ybarmat[:, :, :, :, :, :, np.newaxis], self.card_T, axis=6)
            self.p_x_ybar_z  = self.p_x_ybar_z * p_x_and_ybarmat

            #calculate p(x|zm!=m,ym)
            p_zbar2_y2_x = ((self.p_x_ybar_z.sum(axis=5)).sum(axis=2)).sum(axis=1) #x,y2,,z0,z1
            p_zbar2_y2 = p_zbar2_y2_x.sum(axis=0) #y2,,z0,z1
            p_zbar2_y2 = (((np.repeat(p_zbar2_y2[:,:,:,np.newaxis],self.card_X,axis =3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3) #x,y2,,z0,z1
            p_x_given_zbar2_y2 = np.nan_to_num((p_zbar2_y2_x/p_zbar2_y2),nan=0,posinf=0) #x,y2,,z0,z1
            p_x_given_zbar2_y2 = (np.repeat(p_x_given_zbar2_y2[:,:,:,:,np.newaxis],self.card_T,axis=4))# Repeat for z2 and allign to x,y2,z0,z1,z2

            #Calculate DKL
            p_x_zbar = ((self.p_x_ybar_z.sum(axis=3)).sum(axis=2)).sum(axis=1) #x,z0,,z1,z2
            p_zbar3d = p_x_zbar.sum(axis=0) #z0,z1,z2
            p_zbar = (((np.repeat(p_zbar3d[:,:,:,np.newaxis],self.card_X,axis = 3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3) #x,z0,z1,z2
            p_x_given_zbar = np.nan_to_num((p_x_zbar/p_zbar),nan=0,posinf=0)#x,z0,z1,z2
            p_x_given_zbar = (((np.repeat(p_x_given_zbar[:,:,:,:,np.newaxis],self.card_Y,axis = 4)).transpose(0,1,2,4,3)).transpose(0,1,3,2,4)).transpose(0,2,1,3,4) #x,y2,z0,z1,z2

            dkl = np.nan_to_num((np.log2(np.nan_to_num((p_x_given_zbar2_y2/p_x_given_zbar),nan=0,posinf=0))),nan=0,posinf=0)
            dkl = (dkl*p_x_given_zbar2_y2).sum(axis=0) #y2,z0,z1,z2

            #Calculate p(zm!=m|zm,ym)
            p_zbar_ym2 = ((self.p_x_ybar_z.sum(axis=2)).sum(axis=1)).sum(axis=0) #y2,z0,z1,z2
            p_zm2_ym2 = (p_zbar_ym2.sum(axis=2)).sum(axis=1) #y2,z2
            p_zm2_ym2 = np.repeat(p_zm2_ym2[:,:,np.newaxis],self.card_T,axis=2).transpose(0,2,1) #Repeat for z0 and allign to y1,z0,z1
            p_zm2_ym2 = np.repeat(p_zm2_ym2[:, :, :, np.newaxis], self.card_T, axis=3).transpose(0,1,3,2)#Repeat for z1 and allign tp y1,z0,z1,z2
            p_zbarm_given_zmym = np.nan_to_num((p_zbar_ym2/p_zm2_ym2),nan=0,posinf=0)

            #Calculate distance d(ym,zm)
            p_zbarm = p_zbar3d.sum(axis=2) #z0,z1
            p_zm = (p_zbar3d.sum(axis=1)).sum(axis=0) #z2
            p_zm2d = (np.repeat(p_zm[:,np.newaxis],self.card_Y,axis=1)).transpose(1,0)#y2,z2
            p_zbarm = ((np.repeat(p_zbarm[:,:,np.newaxis],self.card_T,axis=2)))# Repeat for z2 and allign to z0,z1,z2
            p_zm_given_zbarm = np.nan_to_num((p_zbar3d/p_zbarm),nan=0,posinf=0) # z0 ,z1 z2
            d_ym_zm = np.nan_to_num(np.log2(p_zm_given_zbarm),nan=0,posinf=0) # z0,z1,z2
            d_ym_zm = (((np.repeat(d_ym_zm[:,:,:,np.newaxis],self.card_Y, axis =3)).transpose(0,1,3,2)).transpose(0,2,1,3)).transpose(1,0,2,3)#y2,z0,z1,z2
            d_ym_zm = beta*dkl - d_ym_zm
            d_ym_zm= ((p_zbarm_given_zmym * d_ym_zm).sum(axis=2)).sum(axis=1) #y2,z2
            d_ym_zm = np.nan_to_num((np.log2(p_zm2d)),nan=0,posinf=0) + d_ym_zm
            p_zm_ymnew = p_zm2d * np.exp(-d_ym_zm) #y2,z2
            normalization = p_zm_ymnew.sum(axis=1)
            normalization = np.repeat(normalization[:,np.newaxis],self.card_T,axis=1)
            p_zm_ymnew = p_zm_ymnew/normalization

            epsiter = ((np.square((p_zm_ymnew - p_z2_given_y2))).sum())/(self.card_T*self.card_Y)

            #Update quantizer
            self.p_z2_given_y2 = p_zm_ymnew

            # Calculate MI
            p_y2 = (((p_x_and_ybar).sum(axis=2)).sum(axis=1)).sum(axis=0)
            p_y2 = np.repeat(p_y2[:, np.newaxis], self.card_T, axis=1)
            self.rate = MI(self.p_z2_given_y2*p_y2)

