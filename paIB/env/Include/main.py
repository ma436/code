import numpy as np
from matplotlib import pyplot as plt
from Include import bn
from Include import aibnode
from bn import *
from aibnode import *

card_X = 2
card_Y = 16
card_T = 8
mu1 = -1
mu0 = +1
sigma_y0 = 1
sigma_y1 = 2
sigma_y2 = 1.5
samples = 10000000
nodes = 3
capcaity = [0.9,0.9,0.9]
init_beta = [50,50,50]
epsaiB = 1
# Assume |Y| = 1000 for all quantizers

#Declare p(x)
p_x = np.array(([0.5,0.5]))
means = np.array((mu0,mu1))
sigma = np.array((sigma_y0,sigma_y1, sigma_y2))

# Create p(x,y0,y1,y2)
bn = bayesian4(p_x,means,sigma,card_X,card_Y,samples)

# Create quantizers p(z0|y0) p(z1|y1) p(z2|y2)
p_z_given_ymat = np.zeros((nodes,card_Y,card_T))
q0 = quantizer(card_T,card_Y)
q1 = quantizer(card_T,card_Y)
q2 = quantizer(card_T,card_Y)
p_z_given_ymat[0,:,:] = q0.p_z_given_y
p_z_given_ymat[1,:,:] = q1.p_z_given_y
p_z_given_ymat[2,:,:] = q2.p_z_given_y


epsaiBset = epsaiB +1
aib_iter_count = 0
while(epsaiBset > epsaiB):
    rateconst0 = capcaity[0] + 1;
    beta0 = init_beta[0]
    while (rateconst0 > capcaity[0]): # Set a higher limit for while loop to run atleast once
        bnnode0 = aibnode(0, bn.p_x_and_ybar, beta0, 0.1, p_z_given_ymat)
        p_z_given_ymat[0, :, :] = bnnode0.p_z0_given_y0
        rateconst0 = bnnode0.rate
        # Code bisection search
        beta0 = beta0 - 1
    rateconst1 = capcaity[1] + 1; # Set a higher limit for while loop to run atleast once
    beta1 = init_beta[1]
    while (rateconst1 > capcaity[1]):
        bnnode1 = aibnode1(0, bn.p_x_and_ybar, 20, 0.1, p_z_given_ymat)
        p_z_given_ymat[1, :, :] = bnnode1.p_z1_given_y1
        rateconst1 = bnnode1.rate
        # Code bisection search (to do)
        beta1 = beta1 - 1

    rateconst2 = capcaity[2] +1 # Set a higher limit for while loop to run atleast once
    beta2 = init_beta[2]
    while (rateconst2 > capcaity[2]):
        bnnode2 = aibnode2(0, bn.p_x_and_ybar, 20, 0.1, p_z_given_ymat)
        p_z_given_ymat[2, :, :] = bnnode2.p_z2_given_y2
        if (bnnode2.rate > capcaity[2]):
            # Code bisection search (to do)
            beta2 = beta2 - 1
        rateconst2 = bnnode2.rate

    #Check DKL from previous quantizers
    DKL0 = DKL(p_z_given_ymat[0, :, :].T,q0.p_z_given_y.T) # Take transpose to allign according to DKL() requirement
    DKL1 = DKL(p_z_given_ymat[1, :, :].T, q1.p_z_given_y.T)
    DKL2 = DKL(p_z_given_ymat[2, :, :].T, q2.p_z_given_y.T)
    epsaiBset = (DKL0 +DKL1 +DKL2)/3

    #Update quantizers
    q0.p_z_given_y = p_z_given_ymat[0, :, :]
    q1.p_z_given_y= p_z_given_ymat[1,:,:]
    q2.p_z_given_y = p_z_given_ymat[2,:,:]

    aib_iter_count = aib_iter_count +1





plt.plot(bn.p_y0_given_x[0, :], label="PDF")
plt.plot(bn.p_y1_given_x[0, :])
plt.plot(bn.p_y2_given_x[0, :])
plt.legend()
plt.show()
